// pages/detail/detail.js
import { householdFoodWaste, residualWaste, recyclableWaste, hazardousWaste } from '../../api/detail.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: null,
    householdFoodWaste: {},
    residualWaste: {},
    recyclableWaste: {},
    hazardousWaste: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const { id } = options
    switch(id) {
      case '1':
        this.setData({
          detail: householdFoodWaste
        })
        break
      case '2':
        this.setData({
          detail: residualWaste
        })
        break
      case '3':
        this.setData({
          detail: recyclableWaste
        })
        break
      case '4':
        this.setData({
          detail: hazardousWaste
        })
      default:
        break
    }
    // 动态设置当前页面的标题
    wx.setNavigationBarTitle({
      title: this.data.detail.name
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})