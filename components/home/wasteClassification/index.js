// components/home/waste/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    wastes: Array
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onTap(e) {
      // 此处需要使用currentTarget，而不能使用target
      // target触发事件的组件的一些属性值集合
      // currentTarget当前组件的一些属性值集合
      const { id } = e.currentTarget.dataset
      wx.navigateTo({
        url: `/pages/detail/detail?id=${id}`,
      })
    }
  }
})
