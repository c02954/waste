const householdFoodWaste = {
  name: '湿垃圾',
  imageSrc: '/images/common/icon_household_food_waste.png',
  description: '是指易腐的生物质废弃物。主要包括剩菜剩饭、瓜皮果核、花卉绿植、肉类碎骨、过期食品、餐厨垃圾等。',
  requirements: [
    '纯流质的食品垃圾，如牛奶，应直接倒进下水口',
    '有包装物的湿垃圾应将包装物取出后分类投放，包装物请投放到对应的可回收物容器或干垃圾容器',
    '投放湿垃圾时，鼓励奖包装物（如塑料袋）去除'
  ]
}

const residualWaste = {
  name: '干垃圾',
  imageSrc: '/images/common/icon_residual_waste.png',
  description: '是指除有害垃圾、可回收物、湿垃圾意外的其他生活废弃物。',
  requirements: [
    '尽量沥干水分',
    '难以辨识类别的生活垃圾投入干垃圾容器内'
  ]
}

const recyclableWaste = {
  name: '可回收物',
  imageSrc: '/images/common/icon_recyclable_waste.png',
  description: '是指适宜回收和可循环再利用的废弃物。主要包括废玻璃、废金属、废塑料、废纸张、废织物等。',
  requirements: [
    '轻投轻放',
    '清洁干燥，避免污染',
    '废纸尽量平整',
    '立体包装请清空内容物，清洁后压扁投放',
    '有尖锐边角的，应包裹后投放'
  ]
}

const hazardousWaste = {
  name: '有害垃圾',
  imageSrc: '/images/common/icon_hazardous_waste.png',
  description: '是指对人体健康或者自然环境造成直接或者潜在危害的零星废弃物，单位集中产生的除外。主要包括废电池、废灯管、废药品、废油漆桶等。',
  requirements: [
    '充电电池、纽扣电池、蓄电池投放时请注意轻放',
    '油漆桶、杀虫剂如有残留请密闭后投放',
    '荧光灯、节能灯易破损请连带包装或者包裹后轻放',
    '废药品及其包装连带包装一起投放'
  ]
}

export {
  householdFoodWaste,
  residualWaste,
  recyclableWaste,
  hazardousWaste
}