const wastes = [
  {
    id: 1,
    name: "湿垃圾",
    enName: "HOUSEHOLD FOOD WASTE",
    imageSrc: "/images/common/icon_household_food_waste.png"
  },
  {
    id: 2,
    name: "干垃圾",
    enName: "RESIDUAL WASTE",
    imageSrc: "/images/common/icon_residual_waste.png"
  },
  {
    id: 3,
    name: "可回收物",
    enName: "RECYCLABLE WASTE",
    imageSrc: "/images/common/icon_recyclable_waste.png"
  },
  {
    id: 4,
    name: "有害垃圾",
    enName: "HAZARDOUS WASTE",
    imageSrc: "/images/common/icon_hazardous_waste.png"
  }
]

const images = [
  'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1561781634627&di=80f58094b6332e29225287c41e28ccb0&imgtype=0&src=http%3A%2F%2Fhiphotos.baidu.com%2Ffeed%2Fpic%2Fitem%2F0b55b319ebc4b74571d73deac3fc1e178b82158e.jpg',
  'http://sh.eastday.com/images/thumbnailimg/month_1906/5a816c774ce748bb983370330b561849.jpg'
]

export {
  wastes,
  images
}